/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package negocio;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import javax.swing.JPanel;
import util.Secuencia;

/**
 *
 * @author OMAR
 * @param <T>
 */
public class Secuencias<T extends Comparable<T>> extends JPanel {

    private Elemento[] elementos;
    private Secuencia<T> secuencia;
    

    public Secuencias(Secuencia<T> secuencia) {
        this.secuencia = secuencia;
        
        this.elementos = new Elemento[secuencia.getTamanio()];
        
        for (int i = 0; i < secuencia.getTamanio(); i++) {
            elementos[i] = new Elemento(secuencia.get(i).toString());
        }
   
                
    }

    @Override
    protected void paintComponent(Graphics g) {

        super.paintComponent(g);

        Graphics2D g2d = (Graphics2D) g;

        dibujar(g2d);

    }

    public void dibujar(Graphics2D g2d) {
        
//        g2d.setColor(new Color(51, 153, 255));
        int x = 0;
        int y = 0;

        for (Elemento e : elementos) {           
            g2d.draw(e.getElemento(x, y));
            g2d.drawString(e.getString(), x+5, 15);
            x += 30;
        }

    }

}
