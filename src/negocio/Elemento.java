/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package negocio;

import java.awt.geom.Rectangle2D;
import java.awt.geom.RoundRectangle2D;

/**
 *
 * @author OMAR
 * @param <T>
 */
public class Elemento<T extends Comparable<T>> {
    
    private final T element;
    
    public static final int ANCHO = 20;
    public static final int ALTO = 20;

    public Elemento(T element) {
        this.element = element;
    }
    
    public Rectangle2D getElemento(int x, int y) {
        return new Rectangle2D.Double(x, y, ANCHO, ALTO);
    }
    
    public String getString() {
        return element.toString();
    }
}
