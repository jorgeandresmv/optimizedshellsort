package negocio;

import util.Secuencia;

/**
 *
 * @author Nocsabe
 */
public class Sort {

    private Sort() {
    }

    public static <T extends Comparable<T>> Secuencia<T> shellSort(Secuencia<T> array) {
        int j;
        T val;
        int x = array.getTamanio() - 1;
        for (int i = 0; i < x; i++, x--) {
            if (array.get(i).compareTo(array.get(x)) > 0) {
                T temp = array.get(i);
                array.set(i, array.get(x));
                array.set(x, temp);
            }
        }
        for (int gap = array.getTamanio() / 2; gap > 0; gap /= 2) {
            for (int i = gap; i < array.getTamanio(); i++) {
                val = array.get(i);
                for (j = i; j >= gap && array.get(j - gap).compareTo(val) > 0; j -= gap) {
                    array.set(j, array.get(j - gap));
                }
                array.set(j, val);
            }
        }

        return array;
    }

}
