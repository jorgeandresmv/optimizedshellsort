package animacion;

import java.awt.BasicStroke;
import java.awt.Graphics;
import java.awt.Graphics2D;
import javax.swing.JPanel;
import negocio.Sort;
import util.Secuencia;

/**
 *
 * @author OMAR
 * @param <T>
 */
public class Contenedor<T extends Comparable<T>> extends JPanel implements Runnable {

    private final Secuencia<T> secuencia;
    private final Secuencia<T> sec;
    private final Secuencia<Celda<T>> celdas;

    private final int h_max = 350;
    private final int w_max = 768;

    public Contenedor(Secuencia<T> secuencia, Secuencia<T> sec) {
        this.secuencia = secuencia;
        this.sec = sec;
        this.celdas = new Secuencia<>(secuencia.getTamanio());
        inicializar();
    }

    private void inicializar() {
        Celda c;
        int h = h_max / secuencia.getTamanio();
        int w = w_max / secuencia.getTamanio();

        for (int i = 0; i < secuencia.getTamanio(); i++) {
            c = new Celda(secuencia.get(i), 0, 0);
            celdas.insertar(c);
        }

        Secuencia<T> clonOrdenado = Sort.shellSort(clonarSecuencia(secuencia));
        for (int i = 0; i < clonOrdenado.getTamanio(); i++) {
            c = new Celda(clonOrdenado.get(i), 0, 0);
            c = celdas.get(celdas.getIndice(c));
            c.setH((h * i) + 15);
            c.setW(w);
        }
    }

    private Secuencia<T> clonarSecuencia(Secuencia<T> s) {
        Secuencia<T> c = new Secuencia<>(s.getTamanio());

        for (int i = 0; i < s.getTamanio(); i++) {
            c.insertar(s.get(i));
        }

        return c;
    }

    @Override
    public void run() {
        int j;
        T val;
        Celda celdaActual;
        int x = secuencia.getTamanio() - 1;
        for (int i = 0; i < x; i++, x--) {
            if (secuencia.get(i).compareTo(secuencia.get(x)) > 0) {
                T temp = secuencia.get(i);
                celdaActual = celdas.get(i);
                celdaActual.marcar();
                this.repaint();
                secuencia.set(i, secuencia.get(x));
                secuencia.set(x, temp);
                celdas.get(x).activar();
                celdas.get(i).activar();
                this.repaint();
                try {
                        Thread.sleep(200);
                    } catch (InterruptedException ie) {
                    }
                celdas.set(i, celdas.get(x));
                celdas.set(x, celdaActual);
                celdas.get(x).desactivar();
                celdas.get(i).desactivar();
                this.repaint();
            }
        }
        for (int gap = secuencia.getTamanio() / 2; gap > 0; gap /= 2) {
            for (int i = gap; i < secuencia.getTamanio(); i++) {
                val = secuencia.get(i);
                celdaActual = celdas.get(i);
                celdaActual.marcar();
                this.repaint();
                for (j = i; j >= gap && secuencia.get(j - gap).compareTo(val) > 0; j -= gap) {

                    celdas.get(j - gap).activar();
                    celdas.get(j).activar();
                    secuencia.set(j, secuencia.get(j - gap));

                    this.repaint();

                    try {
                        Thread.sleep(200);
                    } catch (InterruptedException ie) {
                    }
                    celdas.get(j - gap).desactivar();
                    celdas.get(j).desactivar();
                    celdas.set(j, celdas.get(j - gap));
                    this.repaint();
                }
                secuencia.set(j, val);
                celdas.set(j, celdaActual);
                celdaActual.desactivar();
                this.repaint();
            }
        }
        
        long inicio = System.currentTimeMillis();
        Sort.shellSort(sec);
        long fin = System.currentTimeMillis();
        double tiempo = (double) (fin - inicio);
        String result = "TIEMPO: " + tiempo + " miliseg.\n";
        //System.out.println(result);
    }

    @Override
    protected void paintComponent(Graphics g) {

        super.paintComponent(g);

        Graphics2D g2d = (Graphics2D) g;

        dibujar(g2d);

    }

    public void dibujar(Graphics2D g) {

        Celda celda;
        int x = 0;
        int w = w_max / celdas.getTamanio();
        for (int i = 0; i < celdas.getTamanio(); i++) {
            celda = celdas.get(i);
            g.setStroke(new BasicStroke(celda.getGrosor()));
            g.setColor(celda.getEstado().getColor());
            g.draw(celda.getRectangle(x, h_max - celda.getH()));
            x += w;
        }

    }
}
