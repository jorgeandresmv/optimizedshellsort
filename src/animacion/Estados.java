package animacion;

import java.awt.Color;

/**
 *
 * @author OMAR
 */
public enum Estados {
    
    INACTIVO(new Color(147, 147, 147)),
    ACTIVO(new Color(49, 106, 197)),
    MARCADO(new Color(232, 180, 130));

    private Color color;

    private Estados(Color color) {
        this.color = color;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

}
