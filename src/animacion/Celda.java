package animacion;

import java.awt.geom.Rectangle2D;
import java.util.Objects;

/**
 *
 * @author OMAR
 * @param <T>
 */
public class Celda<T extends Comparable<T>> implements Comparable<T> {

    private T elemento;
    private float grosor;
    private Estados estado;

    private int h;
    private int w;

    public Celda(T elemento, int w, int h) {
        this.elemento = elemento;
        this.grosor = 1;
        this.w = w;
        this.h = h;
        estado = Estados.INACTIVO;
    }

    public Rectangle2D getRectangle(int x, int y) {
        return new Rectangle2D.Double(x, y, w, h);
    }

    public void setEstado(Estados estado) {
        this.estado = estado;
    }

    public void setElemento(T elemento) {
        this.elemento = elemento;
    }

    public T getElemento() {
        return elemento;
    }

    public Estados getEstado() {
        return estado;
    }

    public float getGrosor() {
        return grosor;
    }

    public void setGrosor(float grosor) {
        this.grosor = grosor;
    }

    public void marcar() {
        this.estado = Estados.MARCADO;
        this.grosor = 5;
    }

    public void activar() {
        this.estado = Estados.ACTIVO;
        this.grosor = 3;
    }

    public void desactivar() {
        this.estado = Estados.INACTIVO;
        this.grosor = 1;
    }

    public void setH(int h) {
        this.h = h;
    }

    public void setW(int w) {
        this.w = w;
    }

    public int getH() {
        return h;
    }

    public int getW() {
        return w;
    }
    
    

    @Override
    public String toString() {
        return elemento.toString();
    }

    @Override
    public int compareTo(T o) {
        return this.elemento.compareTo(o);
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 23 * hash + Objects.hashCode(this.elemento);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Celda<?> other = (Celda<?>) obj;
        if (!Objects.equals(this.elemento, other.elemento)) {
            return false;
        }
        return true;
    }

}
