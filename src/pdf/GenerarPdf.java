/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pdf;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import util.Secuencia;

/**
 *
 * @author OMAR
 * @param <T>
 */
public class GenerarPdf<T extends Comparable<T>> {

    private final Secuencia<T> secuencia;
    private final Secuencia<Celda<T>> celdas;

    public GenerarPdf(Secuencia<T> secuencia) {
        this.secuencia = secuencia;
        this.celdas = new Secuencia<>(secuencia.getTamanio());
        inicializar();
    }

    private void inicializar() {
        Celda c;

        for (int i = 0; i < secuencia.getTamanio(); i++) {
            c = new Celda(secuencia.get(i), 0, 0);
            celdas.insertar(c);
        }

    }

    public String generar() {

        try {
            File f = new File("pdf-genarador.pdf");
            Document document = new Document();
            try {
                PdfWriter.getInstance(document, new FileOutputStream(f));
            } catch (FileNotFoundException fileNotFoundException) {
                System.out.println("(No se encontró el fichero para generar el pdf)" + fileNotFoundException);
            }

            document.addTitle("Metodos de ordenamiento Shell Sort");
            document.addSubject("Exportación de pasos del metodo de ordenamiento Shell Sort");
            document.addAuthor("Omar, Jorge & William");
            document.addCreator("Omar, Jorge & William estudiantes de la UFPS");
            document.open();

            v(document);

            document.close();
            System.out.println("¡Se ha generado tu hoja PDF!");
            return f.getAbsolutePath();
        } catch (DocumentException documentException) {
            System.out.println("Se ha producido un error al generar un documento: " + documentException);
        }
        
        return null;
    }

    private void v(Document d) throws DocumentException {

        Font f = new Font();
        f.setFamily(Font.FontFamily.COURIER.name());
        f.setStyle(Font.NORMAL);
        f.setSize(8);

        int j;
        T val;
        Celda celdaActual;
        int x = secuencia.getTamanio() - 1;
        for (int i = 0; i < x; i++, x--) {
            if (secuencia.get(i).compareTo(secuencia.get(x)) > 0) {
                T temp = secuencia.get(i);
                celdaActual = celdas.get(i);
                secuencia.set(i, secuencia.get(x));
                secuencia.set(x, temp);
                celdas.get(x).activar();
                celdas.get(i).activar();
                this.pintar(d, celdas, f, false);
                try {
                        Thread.sleep(200);
                    } catch (InterruptedException ie) {
                    }
                celdas.set(i, celdas.get(x));
                celdas.set(x, celdaActual);
                celdas.get(x).desactivar();
                celdas.get(i).desactivar();
                this.pintar(d, celdas, f, false);
            }
        }

        for (int gap = secuencia.getTamanio() / 2; gap > 0; gap /= 2) {
            for (int i = gap; i < secuencia.getTamanio(); i++) {
                val = secuencia.get(i);
                celdaActual = celdas.get(i);
                celdaActual.marcar();
                for (j = i; j >= gap && secuencia.get(j - gap).compareTo(val) > 0; j -= gap) {

                    celdas.get(j - gap).activar();
                    celdas.get(j).activar();
                    secuencia.set(j, secuencia.get(j - gap));

                    this.pintar(d, celdas, f, false);

                    celdas.get(j - gap).desactivar();
                    celdas.get(j).desactivar();
                    celdas.set(j, celdas.get(j - gap));
                }
                secuencia.set(j, val);
                celdas.set(j, celdaActual);
                celdaActual.desactivar();
            }
        }
        this.pintar(d, celdas, f, true);
    }

    private void pintar(Document d, Secuencia<Celda<T>> s, Font f, boolean ultimo) throws DocumentException {
        PdfPTable table = new PdfPTable(s.getTamanio());

        table.setWidthPercentage(100);

        PdfPCell cell;

        Celda c;

        for (int i = 0; i < s.getTamanio(); i++) {

            c = s.get(i);

            cell = new PdfPCell(new Phrase(c.toString(), f));
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setFixedHeight(12f);
            if (!ultimo) {
                cell.setBorderColor(BaseColor.BLACK);
                cell.setBackgroundColor(c.getEstado().getColor());
                cell.setBorderWidth(c.getGrosor());
            } else {
                cell.setBackgroundColor(BaseColor.YELLOW);
            }
            table.addCell(cell);

        }

        d.add(table);
    }

//    public static void main(String[] args) {
//
//        String[] datos = "20,19,18,17,16,15,14,31,13,12,11,10,9,8,7,6,5,4,3,2,1,32,33,34,35,36,37,38,39,40".split(",");
//        Secuencia<Integer> s = new Secuencia<>(datos.length);
//        for (String dato : datos) {
//            s.insertar(Integer.parseInt(dato));
//        }
//        new GenerarPdf(s).generar();
//    }

}
